#include "Networks.h"
#include "ReplicationManagerServer.h"

class DeliveryDelegateReplication : public DeliveryDelegate
{
public:

	bool used = false;
	ReplicationManagerServer *replicationManager = nullptr;
	ReplicationCommand replicationCommands[MAX_NETWORK_OBJECTS];
	uint32 replicationCommandCount = 0;

	void onDeliverySuccess(DeliveryManager *) override
	{
		for (uint32 i = 0; i < replicationCommandCount; ++i)
		{
			ReplicationCommand &replicationCommand = replicationCommands[i];

			if (replicationCommand.action == ReplicationAction::Create)
			{
				// Nothing to do
			}
			else if (replicationCommand.action == ReplicationAction::Update)
			{
				// Nothing to do
			}
			else if (replicationCommand.action == ReplicationAction::Destroy)
			{
				replicationManager->removeFromReplication(replicationCommand.networkId);
			}
		}

		used = false;
		replicationManager = nullptr;
		replicationCommandCount = 0;
	}

	void onDeliveryFailure(DeliveryManager *) override
	{
		for (uint32 i = 0; i < replicationCommandCount; ++i)
		{
			ReplicationCommand &replicationCommand = replicationCommands[i];

			if (replicationCommand.action == ReplicationAction::Create)
			{
				replicationManager->create(replicationCommand.networkId);
			}
			else if (replicationCommand.action == ReplicationAction::Update)
			{
				replicationManager->update(replicationCommand.networkId);
			}
			else if (replicationCommand.action == ReplicationAction::Destroy)
			{
				replicationManager->destroy(replicationCommand.networkId);
			}
		}

		used = false;
		replicationManager = nullptr;
		replicationCommandCount = 0;
	}
};

DeliveryDelegateReplication deliveryDelegates[512];

void ReplicationManagerServer::create(uint32 networkId)
{
	// Set a configuration command to create this object

	if (findReplicationCommandForNetworkId(networkId) == nullptr)
	{
		ReplicationCommand command;
		command.action = ReplicationAction::Create;
		command.networkId = networkId;
		replicationCommands[replicationCommandCount++] = command;

		sortReplicationCommandsByNetworkId();

		pendingChangesCount++;
	}
}

void ReplicationManagerServer::update(uint32 networkId)
{
	// Set a configuration command to update this object

	ReplicationCommand *command = findReplicationCommandForNetworkId(networkId);

	if (command != nullptr && command->action == ReplicationAction::None)
	{
		command->action = ReplicationAction::Update;
		pendingChangesCount++;
	}
}

void ReplicationManagerServer::destroy(uint32 networkId)
{
	// Set a configuration command to destroy this object

	ReplicationCommand *command = findReplicationCommandForNetworkId(networkId);

	if (command != nullptr)
	{
		if (command->action == ReplicationAction::None)
		{
			pendingChangesCount++;
		}

		command->action = ReplicationAction::Destroy;
	}
}

void ReplicationManagerServer::setLastInputSequenceNumber(uint32 inputSequenceNumber)
{
	lastInputSequenceNumber = inputSequenceNumber;
}

void ReplicationManagerServer::removeFromReplication(uint32 networkId)
{
	ReplicationCommand *replicationCommand = findReplicationCommandForNetworkId(networkId);

	if (replicationCommand != nullptr)
	{
		*replicationCommand = {};
		removeEmptyReplicationCommands();
	}
}

bool ReplicationManagerServer::hasPendingChanges() const
{
	return pendingChangesCount > 0;
}

void ReplicationManagerServer::write(OutputMemoryStream &packet, Delivery &delivery)
{
	packet << lastInputSequenceNumber;
	replicateNetworkGameObjects(packet, delivery);
}

ReplicationCommand * ReplicationManagerServer::findReplicationCommandForNetworkId(uint32 networkId)
{
#if 0
	// Binary search
	int32 begin = 0;
	int32 end = (int32)replicationCommandCount - 1;
	while (begin <= end)
	{
		int32 middle = (begin + end) / 2;
		uint32 currNetworkId = replicationCommands[middle].networkId;
		if (currNetworkId < networkId)
		{
			begin = middle + 1;
		}
		else if (currNetworkId > networkId)
		{
			end = middle - 1;
		}
		else // (currNetworkId == networkId)
		{
			return &replicationCommands[middle];
		}
	}
#else
	// Linear search
	for (uint32 i = 0; i < replicationCommandCount; ++i)
	{
		if (replicationCommands[i].networkId == networkId)
		{
			return &replicationCommands[i];
		}
	}
#endif

	return nullptr;
}

void ReplicationManagerServer::removeEmptyReplicationCommands()
{
	uint32 nonEmptyCount = 0;
	uint32 pendingCount = 0;
	for (uint32 i = 0; i < replicationCommandCount; ++i)
	{
		if (replicationCommands[i].networkId != 0)
		{
			if (replicationCommands[i].action != ReplicationAction::None)
			{
				pendingCount++;
			}
			replicationCommands[nonEmptyCount++] = replicationCommands[i];
		}
	}

	for (uint32 i = nonEmptyCount; i < replicationCommandCount; ++i)
	{
		replicationCommands[i] = {};
	}

	replicationCommandCount = nonEmptyCount;
	pendingChangesCount = pendingCount;
}

void ReplicationManagerServer::sortReplicationCommandsByNetworkId()
{
	std::sort(replicationCommands,
		replicationCommands + replicationCommandCount,
		[](ReplicationCommand &a, ReplicationCommand &b) -> bool { return a.networkId < b.networkId; });
}

void ReplicationManagerServer::replicateNetworkGameObjects(OutputMemoryStream & packet, Delivery & delivery)
{
	DeliveryDelegateReplication *deliveryDelegate = nullptr;
	for (DeliveryDelegateReplication &deliveryDelegateCurrent : deliveryDelegates)
	{
		if (deliveryDelegateCurrent.used == false)
		{
			deliveryDelegate = &deliveryDelegateCurrent;
		}
	}
	if (deliveryDelegate != nullptr)
	{
		deliveryDelegate->used = true;
		deliveryDelegate->replicationManager = this;
		for (uint32 i = 0; i < replicationCommandCount; ++i)
		{
			deliveryDelegate->replicationCommands[i] = replicationCommands[i];
		}
		deliveryDelegate->replicationCommandCount = replicationCommandCount;
		delivery.delegate = deliveryDelegate;
	}
	else
	{
		WLOG("No more replication delivery delegates available...");
	}

	for (uint32 i = 0; i < replicationCommandCount; ++i)
	{
		ReplicationCommand &replicationCommand = replicationCommands[i];

		if (replicationCommand.action != ReplicationAction::None)
		{
			GameObject *gameObject = App->modLinkingContext->getNetworkGameObject(replicationCommand.networkId);

			if (gameObject != nullptr || replicationCommand.action == ReplicationAction::Destroy)
			{
				packet << replicationCommand.networkId;
				packet << replicationCommand.action;

				if (replicationCommand.action == ReplicationAction::Create)
				{
					replicateNetworkGameObjectCreate(packet, gameObject);
				}
				else if (replicationCommand.action == ReplicationAction::Update)
				{
					replicateNetworkGameObjectUpdate(packet, gameObject);
				}
				else if (replicationCommand.action == ReplicationAction::Destroy)
				{
					// Nothing else to do actually
					int stub = 0;
				}
				else
				{
					ASSERT(0); // Cannot happen
				}
			}

			// Action was either written or the object did not exist anymore
			replicationCommand.action = ReplicationAction::None;

			pendingChangesCount--;

			ASSERT(pendingChangesCount < replicationCommandCount);
		}
	}
}

void ReplicationManagerServer::replicateNetworkGameObjectCreate(OutputMemoryStream & packet, GameObject * gameObject)
{
	packet << gameObject->position.x;
	packet << gameObject->position.y;
	packet << gameObject->angle;
	packet << gameObject->size.x;
	packet << gameObject->size.y;

	if (gameObject->behaviour != nullptr)
	{
		packet << gameObject->behaviour->type();
		gameObject->behaviour->write(packet);
	}
	else
	{
		packet << BehaviourType::None;
	}

	if (gameObject->sprite != nullptr)
	{
		packet << (uint8)1;
		packet << gameObject->sprite->pivot.x;
		packet << gameObject->sprite->pivot.y;
		packet << gameObject->sprite->color.r;
		packet << gameObject->sprite->color.g;
		packet << gameObject->sprite->color.b;
		packet << gameObject->sprite->color.a;
		packet << gameObject->sprite->order;
		packet << std::string(gameObject->sprite->texture->filename);
	}
	else
	{
		packet << (uint8)0;
	}

	if (gameObject->animation != nullptr)
	{
		packet << (uint8)1;
		packet << gameObject->animation->clip->id;
	}
	else
	{
		packet << (uint8)0;
	}

	//if (gameObject->collider != nullptr && gameObject->collider->type != ColliderType::None)
	//{
	//	packet << gameObject->collider->type;
	//	packet << gameObject->collider->isTrigger;
	//}
	//else
	//{
	//	packet << ColliderType::None;
	//}
}

void ReplicationManagerServer::replicateNetworkGameObjectUpdate(OutputMemoryStream & packet, GameObject * gameObject)
{
	packet << gameObject->position.x;
	packet << gameObject->position.y;
	packet << gameObject->angle;

	if (gameObject->behaviour != nullptr)
	{
		gameObject->behaviour->write(packet);
	}
}
