#include "ModuleNetworkingClient.h"


class DeliveryDelegateInput : public DeliveryDelegate
{
public:

	bool used = false;
	uint32 nextSequenceNumber;
	
	void onDeliverySuccess(DeliveryManager *) override
	{
		// Nothing to do
		used = false;
		uint32 nextInputDataFront = max(App->modNetClient->inputDataFront, nextSequenceNumber);
		ASSERT(nextInputDataFront <= App->modNetClient->inputDataBack);
		App->modNetClient->inputDataFront = nextInputDataFront;
	}

	void onDeliveryFailure(DeliveryManager *) override
	{
		used = false;
	}
};


//////////////////////////////////////////////////////////////////////
// ModuleNetworkingClient public methods
//////////////////////////////////////////////////////////////////////


void ModuleNetworkingClient::setServerAddress(const char * pServerAddress, uint16 pServerPort)
{
	serverAddressStr = pServerAddress;
	serverPort = pServerPort;
}

void ModuleNetworkingClient::setPlayerInfo(const char * pPlayerName, uint8 pSpaceshipType)
{
	playerName = pPlayerName;
	spaceshipType = pSpaceshipType;
}



//////////////////////////////////////////////////////////////////////
// ModuleNetworking virtual methods
//////////////////////////////////////////////////////////////////////

void ModuleNetworkingClient::onStart()
{
	if (!createSocket()) return;

	if (!bindSocketToPort(0)) {
		disconnect();
		return;
	}

	// Create remote address
	serverAddress = {};
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(serverPort);
	int res = inet_pton(AF_INET, serverAddressStr.c_str(), &serverAddress.sin_addr);
	if (res == SOCKET_ERROR) {
		reportError("ModuleNetworkingClient::startClient() - inet_pton");
		disconnect();
		return;
	}

	state = ClientState::Connecting;

	inputDataFront = 0;
	inputDataBack = 0;

	secondsSinceLastHello = 9999.0f;
	secondsSinceLastInputDelivery = 0.0f;
	secondsSinceLastReplicationPacket = 0.0f;
	secondsBetweenReplicationPackets = 0.1f;
	secondsSinceLastPing = 0.0f;
	lastPacketReceivedTime = Time.time;
	secondsSincePlayerDied = 0.0f;
}

void ModuleNetworkingClient::onGui()
{
	if (state == ClientState::Stopped) return;

	if (ImGui::CollapsingHeader("ModuleNetworkingClient", ImGuiTreeNodeFlags_DefaultOpen))
	{
		if (state == ClientState::Connecting)
		{
			ImGui::Text("Connecting to server...");
		}
		else if (state == ClientState::Connected)
		{
			ImGui::Text("Connected to server");

			ImGui::Separator();

			ImGui::Text("Player info:");
			ImGui::Text(" - Id: %u", playerId);
			ImGui::Text(" - Name: %s", playerName.c_str());

			ImGui::Separator();

			ImGui::Text("Spaceship info:");
			ImGui::Text(" - Type: %u", spaceshipType);
			ImGui::Text(" - Network id: %u", networkId);

			vec2 playerPosition = {};
			GameObject *playerGameObject = App->modLinkingContext->getNetworkGameObject(networkId);
			if (playerGameObject != nullptr) {
				playerPosition = playerGameObject->position;
			}
			ImGui::Text(" - Coordinates: (%f, %f)", playerPosition.x, playerPosition.y);

			ImGui::Separator();

			ImGui::Text("Connection checking info:");
			ImGui::Text(" - Ping interval (s): %f", PING_INTERVAL_SECONDS);
			ImGui::Text(" - Disconnection timeout (s): %f", DISCONNECT_TIMEOUT_SECONDS);

			ImGui::Separator();

			ImGui::Text("Input:");
			ImGui::InputFloat("Delivery interval (s)", &inputDeliveryIntervalSeconds, 0.01f, 0.1f, 4);

			ImGui::Separator();

			ImGui::Text("Improved lag handling:");
			ImGui::Checkbox("Client side prediction", &clientSidePredictionEnabled);
			ImGui::Checkbox("Entity interpolation", &entityInterpolationEnabled);
		}
	}
}

void ModuleNetworkingClient::onPacketReceived(const InputMemoryStream &packet, const sockaddr_in &fromAddress)
{
	lastPacketReceivedTime = Time.time;

	uint32 protoId;
	packet >> protoId;
	if (protoId != PROTOCOL_ID) return;

	ServerMessage message;
	packet >> message;

	if (state == ClientState::Connecting)
	{
		if (message == ServerMessage::Welcome)
		{
			packet >> playerId;
			packet >> networkId;

			LOG("ModuleNetworkingClient::onPacketReceived() - Welcome from server");
			state = ClientState::Connected;
		}
		else if (message == ServerMessage::Unwelcome)
		{
			WLOG("ModuleNetworkingClient::onPacketReceived() - Unwelcome from server :-(");
			disconnect();
		}
	}
	else if (state == ClientState::Connected)
	{
		if (message == ServerMessage::Replication)
		{
			if (deliveryManager.processSequenceNumber(packet))
			{
				secondsBetweenReplicationPackets =
					lerp(secondsBetweenReplicationPackets,
						secondsSinceLastReplicationPacket,
						0.3f);
				secondsSinceLastReplicationPacket = 0.0f;

				deliveryManager.processSequenceNumberAcks(packet);

				replicationManager.entityInterpolationEnabled = entityInterpolationEnabled;
				replicationManager.playerNetworkId = networkId;
				replicationManager.read(packet);

				if (clientSidePredictionEnabled && replicationManager.didReadPlayer)
				{
					// Simulate input from current state
					GameObject *playerGameObject = App->modLinkingContext->getNetworkGameObject(networkId);
					if (playerGameObject != nullptr)
					{
						InputPacketData previousInputPacketData = inputData[(replicationManager.lastInputSequenceNumber) % MAX_INPUT_DATA_SIMULTANEOUS_PACKETS];
						InputController stubGamepad;
						InputController gamepad = inputControllerFromInputPacketData(previousInputPacketData, stubGamepad);

						for (uint32 i = replicationManager.lastInputSequenceNumber + 1; i < inputDataBack; ++i)
						{
							InputPacketData currentInputPacketData = inputData[i % MAX_INPUT_DATA_SIMULTANEOUS_PACKETS];
							InputController previousGamepad = gamepad;
							gamepad = inputControllerFromInputPacketData(currentInputPacketData, previousGamepad);
							playerGameObject->behaviour->onInput(gamepad);
						}
					}
				}
			}
		}
		else if (message == ServerMessage::Ping)
		{
			// Nothing to do: lastPacketTime was already updated
		}
	}
}

void ModuleNetworkingClient::onUpdate()
{
	if (state == ClientState::Stopped) return;

	// Check timeout
	double minAllowedLastPacketFromClientTime = Time.time - (double)DISCONNECT_TIMEOUT_SECONDS;
	if (lastPacketReceivedTime < minAllowedLastPacketFromClientTime)
	{
		disconnect();
		return;
	}

	if (state == ClientState::Connecting)
	{
		secondsSinceLastHello += Time.deltaTime;

		if (secondsSinceLastHello > 0.1f)
		{
			secondsSinceLastHello = 0.0f;

			OutputMemoryStream packet;
			packet << PROTOCOL_ID;
			packet << ClientMessage::Hello;
			packet << playerName;
			packet << spaceshipType;

			sendPacket(packet, serverAddress);
		}
	}
	else if (state == ClientState::Connected)
	{
		GameObject *playerGameObject = App->modLinkingContext->getNetworkGameObject(networkId);

		secondsSinceLastInputDelivery += Time.deltaTime;
		secondsSinceLastReplicationPacket += Time.deltaTime;

		// Check deliveries timeout
		deliveryManager.processTimedOutPackets();

		// Process more inputs if there's space
		if (inputDataBack - inputDataFront < ArrayCount(inputData))
		{
			if (clientSidePredictionEnabled)
			{
				// Simulate current input
				if (playerGameObject != nullptr &&
					playerGameObject->behaviour != nullptr)
				{
					playerGameObject->behaviour->onInput(Input);
				}
			}

			// Pack current input
			uint32 currentInputData = inputDataBack++;
			InputPacketData &inputPacketData = inputData[currentInputData % ArrayCount(inputData)];
			inputPacketData.sequenceNumber = currentInputData;
			inputPacketData.horizontalAxis = Input.horizontalAxis;
			inputPacketData.verticalAxis = Input.verticalAxis;
			inputPacketData.buttonBits = packInputControllerButtons(Input);
		}

		// Input delivery interval timed out: create a new input packet
		if (secondsSinceLastInputDelivery > inputDeliveryIntervalSeconds)
		{
			secondsSinceLastInputDelivery = 0.0f;

			OutputMemoryStream packet;
			packet << PROTOCOL_ID;
			packet << ClientMessage::Input;

			Delivery *delivery = deliveryManager.writeSequenceNumber(packet);

			static DeliveryDelegateInput inputDeliveryDelegates[128] = {};
			for (int i = 0; i < ArrayCount(inputDeliveryDelegates); ++i)
			{
				DeliveryDelegateInput *inputDeliveryDelegate = &inputDeliveryDelegates[i];
				if (inputDeliveryDelegate->used == false)
				{
					inputDeliveryDelegate->used = true;
					inputDeliveryDelegate->nextSequenceNumber = inputDataBack;
					delivery->delegate = inputDeliveryDelegate;
					break;
				}
			}
			if (delivery->delegate == nullptr)
			{
				WLOG("Too many input delivery delegates at the same time.");
			}

			deliveryManager.writePendingSequenceNumberAcks(packet);

			for (uint32 i = inputDataFront; i < inputDataBack; ++i)
			{
				InputPacketData &inputPacketData = inputData[i % ArrayCount(inputData)];
				packet << inputPacketData.sequenceNumber;
				packet << inputPacketData.horizontalAxis;
				packet << inputPacketData.verticalAxis;
				packet << inputPacketData.buttonBits;
			}

			sendPacket(packet, serverAddress);
		}

		// Entity interpolation
		if (entityInterpolationEnabled)
		{
			uint16 count;
			GameObject *networkGameObjects[MAX_NETWORK_OBJECTS];
			App->modLinkingContext->getNetworkGameObjects(networkGameObjects, &count);
			for (uint16 i = 0; i < count; ++i)
			{
				GameObject *gameObject = networkGameObjects[i];
				if (gameObject->networkId != networkId && gameObject->networkInterpolationEnabled)
				{
					const float t = secondsSinceLastReplicationPacket / secondsBetweenReplicationPackets;
					gameObject->position = lerp(gameObject->position, gameObject->finalPosition, t);
					gameObject->angle = lerp(gameObject->angle, gameObject->finalAngle, t);
				}
			}
		}
		else
		{
			uint16 count;
			GameObject *networkGameObjects[MAX_NETWORK_OBJECTS];
			App->modLinkingContext->getNetworkGameObjects(networkGameObjects, &count);
			for (uint16 i = 0; i < count; ++i)
			{
				GameObject *gameObject = networkGameObjects[i];
				if (gameObject->networkId != networkId)
				{
					gameObject->finalPosition = gameObject->position;
					gameObject->finalAngle = gameObject->angle;
				}
			}
		}
		
		// Ping packet
		secondsSinceLastPing += Time.deltaTime;
		if (secondsSinceLastPing > PING_INTERVAL_SECONDS && secondsSinceLastInputDelivery > PING_INTERVAL_SECONDS)
		{
			secondsSinceLastPing = 0.0f;

			OutputMemoryStream packet;
			packet << PROTOCOL_ID;
			packet << ClientMessage::Ping;
			deliveryManager.writePendingSequenceNumberAcks(packet);
			sendPacket(packet, serverAddress);
		}

		// Update camera for player
		if (playerGameObject != nullptr)
		{
			App->modRender->cameraPosition = playerGameObject->position;
		}
		else
		{
			// Disconnect after some time being dead
			secondsSincePlayerDied += Time.deltaTime;
			if (secondsSincePlayerDied > 3.0f)
			{
				NetworkDisconnect();
			}
		}
	}
}

void ModuleNetworkingClient::onConnectionReset(const sockaddr_in & fromAddress)
{
	disconnect();
}

void ModuleNetworkingClient::onDisconnect()
{
	state = ClientState::Stopped;

	deliveryManager.clear();

	GameObject *networkGameObjects[MAX_NETWORK_OBJECTS] = {};
	uint16 networkGameObjectsCount;
	App->modLinkingContext->getNetworkGameObjects(networkGameObjects, &networkGameObjectsCount);
	App->modLinkingContext->clear();

	for (uint32 i = 0; i < networkGameObjectsCount; ++i)
	{
		Destroy(networkGameObjects[i]);
	}

	App->modRender->cameraPosition = {};
}
