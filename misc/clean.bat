
@set CURRENTDIR=%~dp0

@pushd "%CURRENTDIR%.."

@REM Remove compilation files

@if exist x64 ( rd /s /q x64 )
@if exist .vs ( rd /s /q .vs )

@popd

