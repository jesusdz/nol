
@set CURRENTDIR=%~dp0

@pushd "%CURRENTDIR%.."

@REM Compile the full solution (reuses previous compilations)
msbuild /verbosity:minimal Networks.sln

@popd

