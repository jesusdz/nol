#include "Networks.h"
#include "DeliveryManager.h"

Delivery * DeliveryManager::writeSequenceNumber(OutputMemoryStream & packet)
{
	ASSERT(pendingDeliveriesBack - pendingDeliveriesFront < ArrayCount(pendingDeliveries));

	packet << nextOutgoingSequenceNumber;

	Delivery *deliveryProcess = &pendingDeliveries[pendingDeliveriesBack % ArrayCount(pendingDeliveries)];
	deliveryProcess->sequenceNumber = nextOutgoingSequenceNumber;
	deliveryProcess->dispatchTime = Time.time;
	deliveryProcess->delegate = nullptr;

	nextOutgoingSequenceNumber++;
	pendingDeliveriesBack++;
	dispatchedPacketCount++;
	
	return deliveryProcess;
}

bool DeliveryManager::processSequenceNumber(const InputMemoryStream & packet)
{
	uint32 receivedSequenceNumber;
	packet >> receivedSequenceNumber;

	if (receivedSequenceNumber >= nextExpectedSequenceNumber)
	{
		nextExpectedSequenceNumber = receivedSequenceNumber + 1;

		ASSERT(pendingAcksCount < MAX_PENDING_ACKS);
		pendingAcks[pendingAcksCount++] = receivedSequenceNumber;

		return true;
	}
	else
	{
		return false;
	}
}

bool DeliveryManager::hasPendingSequenceNumberAcks() const
{
	return pendingAcksCount > 0;
}

void DeliveryManager::writePendingSequenceNumberAcks(OutputMemoryStream & packet)
{
	if (pendingAcksCount > 0) {
		packet.Write(true);
		packet << pendingAcksCount;
		for (uint32 i = 0; i < pendingAcksCount; ++i)
		{
			packet << pendingAcks[i];
		}
		pendingAcksCount = 0;
	} else {
		packet.Write(false);
	}
}

void DeliveryManager::processSequenceNumberAcks(const InputMemoryStream & packet)
{
	bool hasAcks;
	packet.Read(hasAcks);

	if (hasAcks)
	{
		uint32 incomingAcksCount;
		packet >> incomingAcksCount;

		uint32 incomingAcks[MAX_PENDING_ACKS];
		for (uint32 i = 0; i < incomingAcksCount; ++i) {
			packet >> incomingAcks[i];
		}

		uint32 currentAckIndex = 0;
		while (currentAckIndex < incomingAcksCount && pendingDeliveriesFront < pendingDeliveriesBack)
		{
			uint32 ackedSequenceNumber = incomingAcks[currentAckIndex];

			Delivery &deliveryProcess = pendingDeliveries[pendingDeliveriesFront % ArrayCount(pendingDeliveries)];
			uint32 deliverySequenceNumber = deliveryProcess.sequenceNumber;

			if (ackedSequenceNumber > deliverySequenceNumber)
			{
				pendingDeliveriesFront++;

				droppedPacketCount++;

				if (deliveryProcess.delegate != nullptr) deliveryProcess.delegate->onDeliveryFailure(this);
			}
			else if (ackedSequenceNumber == deliverySequenceNumber)
			{
				deliveredPacketCount++;

				if (deliveryProcess.delegate != nullptr) deliveryProcess.delegate->onDeliverySuccess(this);

				pendingDeliveriesFront++;

				currentAckIndex++;
			}
			else
			{
				// Nothing todo, we have received an ack for something that
				// was already acked or maybe timedout
				currentAckIndex++;
			}
		}
	}
}

void DeliveryManager::processTimedOutPackets()
{
	double timeoutTime = Time.time - (double)PACKET_DELIVERY_TIMEOUT_SECONDS;

	while (pendingDeliveriesFront < pendingDeliveriesBack)
	{
		Delivery &deliveryProcess = pendingDeliveries[pendingDeliveriesFront % ArrayCount(pendingDeliveries)];
		double dispatchTime = deliveryProcess.dispatchTime;

		if (dispatchTime < timeoutTime)
		{
			droppedPacketCount++;
			pendingDeliveriesFront++;
			if (deliveryProcess.delegate != nullptr) deliveryProcess.delegate->onDeliveryFailure(this);
		}
		else
		{
			break;
		}
	}
}

void DeliveryManager::clear()
{
	nextOutgoingSequenceNumber = 0;
	nextExpectedSequenceNumber = 0;

	deliveredPacketCount = 0;
	droppedPacketCount = 0;
	dispatchedPacketCount = 0;

	pendingDeliveriesFront = 0;
	pendingDeliveriesBack = 0;

	pendingAcksCount = 0;
}
