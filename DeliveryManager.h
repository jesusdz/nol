#pragma once

class DeliveryManager;

class DeliveryDelegate
{
public:

	virtual void onDeliverySuccess(DeliveryManager *deliveryManager) = 0;
	virtual void onDeliveryFailure(DeliveryManager *deliveryManager) = 0;
};

struct Delivery
{
	uint32 sequenceNumber = 0;
	double dispatchTime = 0.0;
	DeliveryDelegate *delegate = nullptr;
};

class DeliveryManager
{
public:

	// For senders to write a new seq. numbers into a packet
	Delivery * writeSequenceNumber(OutputMemoryStream &packet);

	// For receivers to process the seq. number from an incoming packet
	bool processSequenceNumber(const InputMemoryStream &packet);

	// For receivers to write ack'ed seq. numbers into a packet
	bool hasPendingSequenceNumberAcks() const;
	void writePendingSequenceNumberAcks(OutputMemoryStream &packet);

	// For senders to process ack'ed seq. numbers from a packet
	void processSequenceNumberAcks(const InputMemoryStream &packet);
	void processTimedOutPackets();

	void clear();

private:

	// Sender side members ////////////////////////////////////

	uint32 nextOutgoingSequenceNumber = 0;

	Delivery pendingDeliveries[1024]; // NOTE(jesus): At least PACKET_DELIVERY_TIMEOUT_SECONDS * estimated packets per second
	uint32 pendingDeliveriesFront = 0;
	uint32 pendingDeliveriesBack = 0;

	uint32 deliveredPacketCount = 0;
	uint32 droppedPacketCount = 0;
	uint32 dispatchedPacketCount = 0;

	// Receiver side members //////////////////////////////////

	uint32 nextExpectedSequenceNumber = 0;

	static const int MAX_PENDING_ACKS = 64;
	uint32 pendingAcks[MAX_PENDING_ACKS];
	uint32 pendingAcksCount = 0;
};

