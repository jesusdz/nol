#pragma once

enum class ReplicationAction
{
	None,
	Create,
	Update,
	Destroy
};

struct ReplicationCommand
{
	ReplicationAction action; // NOTE(jesus): This is the action to replicate to all clients
	uint32 networkId;         // NOTE(jesus): This is the network game object involved
};
