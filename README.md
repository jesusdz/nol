# NOL (Networks and Online Games)

Repository for the Networks and Online Games subject.
All the code in this repository is strictly for academic use.

## Instructions

1. Open Networks.sln in visual studio
2. Click play to execute two instances of the application (start one as client and one as server)
3. Open new instances as client to debug multiple players

## Networking features
* UDP-based socket communication
* Client-server architecture
* Client-side prediction and server reconciliation
* Redundancy-based avoidance of input packet loss
* Custom reliability system (packet delivery notifications)
* Entity interpolation
* Simulation of real world conditions (latency, jitter, and packet loss)

## Others
* No library dependencies other than Dear IMGUI and stb_image
* Uses Microsoft Direct3D 11 native API
