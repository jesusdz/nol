#pragma once

class ReplicationManagerClient
{
public:

	void read(const InputMemoryStream &packet);

	uint32 lastInputSequenceNumber = 0;

	// Client side prediction
	uint32 playerNetworkId = 0;
	bool didReadPlayer = false;

	// Entity interpolation
	bool entityInterpolationEnabled = false;

private:

	void receiveReplicatedNetworkGameObjects(const InputMemoryStream &packet);

	void receiveReplicatedNetworkGameObjectCreate(const InputMemoryStream &packet, GameObject *gameObject);

	void receiveReplicatedNetworkGameObjectUpdate(const InputMemoryStream &packet, GameObject *gameObject);
};
