#pragma once

enum class ClientMessage : uint8
{
	Hello,
	Input,
	Ping
};

enum class ServerMessage : uint8
{
	Welcome,
	Unwelcome,
	Replication,
	Ping
};
