#include "ReplicationManagerClient.h"

void ReplicationManagerClient::read(const InputMemoryStream &packet)
{
	packet >> lastInputSequenceNumber;
	receiveReplicatedNetworkGameObjects(packet);
}

void ReplicationManagerClient::receiveReplicatedNetworkGameObjects(const InputMemoryStream &packet)
{
	didReadPlayer = false;

	while (packet.RemainingByteCount() > 0)
	{
		uint32 networkId;
		packet >> networkId;

		// Client side prediction
		if (networkId == playerNetworkId)
		{
			didReadPlayer = true;
		}

		GameObject *gameObject = App->modLinkingContext->getNetworkGameObject(networkId);

		ReplicationAction action;
		packet >> action;

		if (action == ReplicationAction::Create)
		{
			if (gameObject == nullptr)
			{
				// Just in case there was another object registered (and delete did not arrive in time)
				GameObject *gameObjectUnsafe = App->modLinkingContext->getNetworkGameObject(networkId, false);
				if (gameObjectUnsafe != gameObject)
				{
					App->modLinkingContext->unregisterNetworkGameObject(gameObjectUnsafe);
					Destroy(gameObjectUnsafe);
				}

				// NOTE(jesus): With subclassing + polymorphism each GO would have custom serialization
				gameObject = Instantiate();
				App->modLinkingContext->registerNetworkGameObjectWithNetworkId(gameObject, networkId);
				receiveReplicatedNetworkGameObjectCreate(packet, gameObject);
			}
			else
			{
				// else: this 'create' arrived more than once
				receiveReplicatedNetworkGameObjectCreate(packet, nullptr);
			}
		}
		else if (action == ReplicationAction::Update)
		{
			if (gameObject != nullptr)
			{
				// NOTE(jesus): With subclassing + polymorphism each GO would have custom serialization
				receiveReplicatedNetworkGameObjectUpdate(packet, gameObject);
			}
			else
			{
				// else: this 'update' was received before 'create'
				receiveReplicatedNetworkGameObjectUpdate(packet, nullptr);
			}
		}
		else if (action == ReplicationAction::Destroy)
		{
			if (gameObject != nullptr)
			{
				App->modLinkingContext->unregisterNetworkGameObject(gameObject);
				Destroy(gameObject);
			}
			// else: this 'destroy' arrived more than once
		}
	}
}

void ReplicationManagerClient::receiveReplicatedNetworkGameObjectCreate(const InputMemoryStream &packet, GameObject *gameObject)
{
	if (gameObject == nullptr)
	{
		static GameObject dummy;
		static Sprite dummySprite;
		static Animation dummyAnimation;
		static Collider dummyCollider;
		gameObject = &dummy;
		gameObject->sprite = &dummySprite;
		gameObject->animation = &dummyAnimation;
		gameObject->collider = &dummyCollider;
		gameObject->behaviour = (Behaviour*)1;
	}

	packet >> gameObject->position.x;
	packet >> gameObject->position.y;
	packet >> gameObject->angle;
	packet >> gameObject->size.x;
	packet >> gameObject->size.y;

	BehaviourType behaviourType;
	packet >> behaviourType;
	if (behaviourType != BehaviourType::None && gameObject->behaviour == nullptr)
	{
		gameObject->behaviour = App->modBehaviour->addBehaviour(behaviourType, gameObject);
		gameObject->behaviour->isServer = false;
		gameObject->behaviour->isLocalPlayer = (gameObject->networkId == playerNetworkId);

		gameObject->behaviour->read(packet);
	}

	uint8 hasSprite;
	packet >> hasSprite;

	if (hasSprite)
	{
		if (gameObject->sprite == nullptr)
		{
			gameObject->sprite = App->modRender->addSprite(gameObject);
		}

		packet >> gameObject->sprite->pivot.x;
		packet >> gameObject->sprite->pivot.y;
		packet >> gameObject->sprite->color.r;
		packet >> gameObject->sprite->color.g;
		packet >> gameObject->sprite->color.b;
		packet >> gameObject->sprite->color.a;
		packet >> gameObject->sprite->order;

		std::string textureFilename;
		packet >> textureFilename;
		gameObject->sprite->texture = App->modTextures->loadTexture(textureFilename.c_str());
	}

	uint8 hasAnimation;
	packet >> hasAnimation;

	if (hasAnimation)
	{
		if (gameObject->animation == nullptr)
		{
			gameObject->animation = App->modRender->addAnimation(gameObject);
		}

		uint16 animationClipId;
		packet >> animationClipId;
		gameObject->animation->clip = App->modRender->getAnimationClip(animationClipId);
	}

	//ColliderType colliderType;
	//packet >> colliderType;
	//
	//if (colliderType != ColliderType::None)
	//{
	//	if (gameObject->collider == nullptr)
	//	{
	//		gameObject->collider = App->modCollision->addCollider(colliderType, gameObject);
	//	}

	//	bool isTrigger;
	//	packet >> isTrigger;
	//	gameObject->collider->isTrigger = isTrigger;
	//}

	// For entity interpolation
	if (entityInterpolationEnabled)
	{
		gameObject->finalPosition = gameObject->position;
		gameObject->finalAngle = gameObject->angle;
	}
}

void ReplicationManagerClient::receiveReplicatedNetworkGameObjectUpdate(const InputMemoryStream &packet, GameObject *gameObject)
{
	if (gameObject == nullptr)
	{
		static GameObject dummy;
		gameObject = &dummy;
	}

	vec2 receivedPosition;
	float receivedAngle;
	packet >> receivedPosition.x;
	packet >> receivedPosition.y;
	packet >> receivedAngle;

	if (gameObject->behaviour != nullptr)
	{
		gameObject->behaviour->read(packet);
	}

	if (entityInterpolationEnabled)
	{
		gameObject->finalPosition = receivedPosition;
		gameObject->finalAngle = receivedAngle;

		if (gameObject->networkId == playerNetworkId)
		{
			gameObject->position = receivedPosition;
			gameObject->angle = receivedAngle;
		}
	}
	else
	{
		gameObject->position = receivedPosition;
		gameObject->angle = receivedAngle;
	}
}
