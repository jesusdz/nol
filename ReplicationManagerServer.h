#pragma once

class ReplicationManagerServer
{
public:

	void create(uint32 networkId);
	void update(uint32 networkId);
	void destroy(uint32 networkId);

	void setLastInputSequenceNumber(uint32 inputSequenceNumber);

	void removeFromReplication(uint32 networkId);

	bool hasPendingChanges() const;
	void write(OutputMemoryStream &packet, Delivery &delivery);

private:

	ReplicationCommand *findReplicationCommandForNetworkId(uint32 networkId);
	void removeEmptyReplicationCommands();
	void sortReplicationCommandsByNetworkId();

	void replicateNetworkGameObjects(OutputMemoryStream & packet, Delivery &delivery);

	void replicateNetworkGameObjectCreate(OutputMemoryStream & packet, GameObject * gameObject);

	void replicateNetworkGameObjectUpdate(OutputMemoryStream & packet, GameObject * gameObject);

	// NOTE(jesus): we must only have one replication command per networkId
	ReplicationCommand replicationCommands[MAX_NETWORK_OBJECTS] = {};
	uint32 replicationCommandCount = 0;

	uint32 pendingChangesCount = 0;

	uint32 lastInputSequenceNumber = 0;
};
